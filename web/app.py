from flask import Flask
from flask import render_template, abort
from jinja2 import TemplateNotFound

app = Flask(__name__)

@app.route("/<path:name>")
def hello(name):
    print("name is {}".format(name))

    if name[-3:] == "css" or name[-4:] == "html":
        print("page url: {}".format(name))

        if name.find("~") != -1 or name.find("//") != -1 or name.find("/..") != -1:
            abort(403)
        else:
            try:
                return render_template(name)
            except TemplateNotFound as err:
                abort(404)
    else:
        print("UOCIS docker demo!")

@app.errorhandler(403)
def page_not_found(error):
    return render_template('403.html'), 403

@app.errorhandler(404)
def error_404(error):
    return render_template('404.html'), 404

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')

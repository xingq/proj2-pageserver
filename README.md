# README # 

## Author: Xing Qian, xingq@uoregon.edu ##

This is a solution of the assignment "project2" project for CIS 322

You can view the project description: https://bitbucket.org/UOCIS322/proj2-pageserver/src/master/

## Introduction of my solution

The assignment asked for rebuild the project1 by flask and docker, and I have reach this goal.

## Run the project

```
# go to the web folder, and run the following command.
# at first, we need to build the image
docker build -t uocis-flask-demo .

# run the image
docker run -d -p 5000:5000 uocis-flask-demo

# tests the page
./tests/tests.sh http://127.0.0.1:5000
```

## Details for this assignment

- The assignment asked for creating two html files ( `403.html` and `404.html`), I create them under the `template` folder.
- I also create a test page `test.html` at under the `template\test` folder, and `trivia.css` at the `static` folder.